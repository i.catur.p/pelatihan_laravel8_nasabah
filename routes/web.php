<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/blog', 'BlogController@home');
Route::get('/blog/tentang', 'BlogController@tentang');
Route::get('/blog/kontak', 'BlogController@kontak');

Route::get('/Nasabah', 'NasabahController@index');
Route::get('/Nasabah/tambah', 'NasabahController@tambah');
Route::post('/Nasabah/store', 'NasabahController@store');
Route::get('/Nasabah/edit/{id}', 'NasabahController@edit');
Route::post('/Nasabah/update', 'NasabahController@update');
Route::get('/Nasabah/hapus/{id}', 'NasabahController@hapus');