<!DOCTYPE html>
<html>
<head>
	<title>Pelatihan Membuat CRUD Pada Laravel</title>
</head>
<body>
 
	<h3>Data nasabah</h3>
 
	<a href="/Nasabah/tambah"> + Tambah nasabah Baru</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Email</th>
			<th>tanggal lahir</th>
			<th>Opsi</th>
		</tr>
		@foreach($nasabah as $p)
		<tr>
			<td>{{ $p->nasabah_nama }}</td>
			<td>{{ $p->nasabah_alamat }}</td>
			<td>{{ $p->nasabah_email }}</td>
			<td>{{ $p->nasabah_tgl_lahir }}</td>
			<td>
				<a href="/Nasabah/edit/{{ $p->nasabah_id }}">Edit</a>
				|
				<a href="/Nasabah/hapus/{{ $p->nasabah_id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
	<br>
	Halaman : {{$nasabah->currentPage()}}
	<br>
	Jumlah Data : {{$nasabah->total()}}
	<br>
	Data Perhalaman : {{$nasabah->perPage()}}
	<br>
	{{$nasabah->links() }}
 
 
</body>
</html>