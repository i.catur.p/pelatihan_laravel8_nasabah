<!DOCTYPE html>
<html>
<head>
   <title>Pelatihan Membuat CRUD Pada Laravel<</title>
</head>
<body>
   <h3>Data nasabah</h3>
   <a href="/Nasabah"> Kembali</a>

   <!-- Menampilkan error validasi -->
  @if (count($errors) > 0)
  <div class="alert alert-danger">
   <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
   </ul>
  </div>
  @endif
  <!-- Akhir Menampilkan error validasi -->
   <br/>
   <br/>
   <form action="/Nasabah/store" method="post">
       {{ csrf_field() }}
       <label for="nama">Nama:</label><br>
       <input type="text" id="nama" name="nama"><br>
       <label for="alamat">Alamat:</label><br>
       <input type="text" id="alamat" name="alamat"><br>
       <label for="email">Email:</label><br>
       <input type="text" id="email" name="email"><br>
       <label for="tanggal_lahir">Tanggal Lahir:</label><br>
       <input type="date" id="tanggal_lahir" name="tanggal_lahir"><br>
       <input type="submit" value="Simpan Data">
   </form>
</body>
</html>