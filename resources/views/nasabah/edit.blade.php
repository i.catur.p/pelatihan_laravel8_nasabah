<!DOCTYPE html>
<html>
<head>
  <title>Pelatihan Membuat CRUD Pada Laravel<</title>
</head>
<body>
  <h3>Edit nasabah</h3>
  <a href="/Nasabah"> Kembali</a>
   <br/>
  <br/>
  @foreach($nasabah as $p)
  <form action="/Nasabah/update" method="post">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $p->nasabah_id }}"> <br/>
      <label for="nama">Nama:</label><br>
      <input type="text" id="nama" name="nama" value="{{ $p->nasabah_nama }}"><br>
      <label for="alamat">Alamat:</label><br>
      <input type="text" id="alamat" name="alamat" value="{{ $p->nasabah_alamat }}"><br>
      <label for="email">Email:</label><br>
      <input type="text" id="email" name="email" value="{{ $p->nasabah_email }}"><br>
      <label for="tanggal_lahir">Tanggal Lahir:</label><br>
      <input type="date" id="tanggal_lahir" name="tanggal_lahir" value="{{ $p->nasabah_tgl_lahir }}"</input><br>
      <input type="submit" value="Simpan Data">
  </form>
  @endforeach
</body>
</html>