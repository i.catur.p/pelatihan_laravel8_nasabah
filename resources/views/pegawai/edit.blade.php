<!DOCTYPE html>
<html>
<head>
  <title>Pelatihan Membuat CRUD Pada Laravel<</title>
</head>
<body>
  <h3>Edit Pegawai</h3>
  <a href="/pegawai"> Kembali</a>
   <br/>
  <br/>
  @foreach($pegawai as $p)
  <form action="/pegawai/update" method="post">
      {{ csrf_field() }}
      <input type="hidden" name="id" value="{{ $p->pegawai_id }}"> <br/>
      <label for="nama">Nama:</label><br>
      <input type="text" id="nama" name="nama" value="{{ $p->pegawai_nama }}"><br>
      <label for="jabatan">Jabatan:</label><br>
      <input type="text" id="jabatan" name="jabatan" value="{{ $p->pegawai_jabatan }}"><br>
      <label for="umur">Umur:</label><br>
      <input type="number" id="umur" name="umur" value="{{ $p->pegawai_umur }}"><br>
      <label for="alamat">Alamat:</label><br>
      <textarea type="text" id="alamat" name="alamat">{{ $p->pegawai_alamat }}</textarea><br>
      <input type="submit" value="Simpan Data">
  </form>
  @endforeach
</body>
</html>