<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NasabahController extends Controller
{
    public function index()
    {
        // mengambil data dari table nasabah
        // $nasabah = DB::table('nasabah')->get();
        $dd = DB::table('nasabah')->paginate(5);

        // mengirim data nasabah ke view index
        // return view('nasabah.index',['nasabah' => $nasabah]);
        return view('nasabah.index', ['nasabah' => $dd]);
    }
    public function tambah()
    {
        // memanggil view tambah
        return view('nasabah.tambah');
    }
    public function store(Request $request)
    {
        // insert data ke table nasabah
        $this->validate($request, [
            'nama' => 'required|min:5|max:20',
            'alamat' => 'required',
            'email' => 'required',
            'tanggal_lahir' => 'required'
        ]);
        DB::table('nasabah')->insert([
            'nasabah_nama' => $request->nama,
            'nasabah_alamat' => $request->alamat,
            'nasabah_email' => $request->email,
            'nasabah_tgl_lahir' => $request->tanggal_lahir
        ]);
        // alihkan halaman ke halaman nasabah
        return redirect('/Nasabah');
    }
    // method untuk edit data nasabah
    public function edit($id)
    {
        // mengambil data nasabah berdasarkan id yang dipilih
        $nasabah = DB::table('nasabah')->where('nasabah_id', $id)->get();
        // passing data nasabah yang didapat ke view edit.blade.php
        return view('nasabah.edit', ['nasabah' => $nasabah]);
    }

    public function update(Request $request)
    {
        // update data nasabah
        DB::table('nasabah')->where('nasabah_id', $request->id)->update([
            'nasabah_nama' => $request->nama,
            'nasabah_alamat' => $request->alamat,
            'nasabah_email' => $request->email,
            'nasabah_tgl_lahir' => $request->tanggal_lahir
        ]);
        // alihkan halaman ke halaman nasabah
        return redirect('/Nasabah');
    }

    public function hapus($id)
    {
        // hapus data nasabah
        DB::table('nasabah')->where('nasabah_id', $id)->delete();
        // alihkan halaman ke halaman nasabah
        return redirect('/Nasabah');
    }
}